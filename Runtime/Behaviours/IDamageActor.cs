using System;

namespace Deaven.CombatSystem
{
    public interface IDamageActor
    {
        public event Action Attacked;
        public void Attack(IDamageable target);
        public bool CanAttack(IDamageable target);
    }
}