using System;

namespace Deaven.CombatSystem
{
    public interface IDamageable
    {
        public bool IsAlive { get; set; }
        public int Health { get; set; }
        public int MaxHealth { get; set; }
        public float InvulnerabilityTime { get; set; }
        public bool IsInvulnerable { get; set; }

        public event Action HealthChanged;
        public event Action Healed;
        public event Action Revived;
        public event Action Damaged;
        public event Action MaxHealthChanged;
        public event Action Killed;

        public void Heal(DamageMessage data);
        public void Revive();
        public void TakeDamage(DamageMessage data);
        public void SetMaxHealth(int value);
        public void IncreaseMaxHealth(int value);
        public void DecreaseMaxHealth(int value);
        public void Kill();
    }
}