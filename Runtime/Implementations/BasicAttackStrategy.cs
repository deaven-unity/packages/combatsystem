using System;
using UnityEngine;

namespace Deaven.CombatSystem
{
    public class BasicAttackStrategy : IDamageActor
    {
        public float Cooldown;
        
        [SerializeField]
        private int _baseDamage = 10;

        private float _nextAttack;

        public event Action Attacked;

        public void Attack(IDamageable target)
        {
            if (!CanAttack(target))
                return;

            DamageMessage msg = new DamageMessage
            {
                DamageActor = this,
                Amount = _baseDamage,
            };

            target.TakeDamage(msg);

            _nextAttack = Time.time + Cooldown;

            Attacked?.Invoke();
        }

        public bool CanAttack(IDamageable target) => Time.time > _nextAttack;
    }
}