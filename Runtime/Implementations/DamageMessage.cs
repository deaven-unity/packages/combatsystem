using UnityEngine;

namespace Deaven.CombatSystem
{
    public struct DamageMessage
    {
        public IDamageActor DamageActor;
        public int Amount;
    }
}