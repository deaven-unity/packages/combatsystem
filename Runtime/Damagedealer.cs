using Sirenix.OdinInspector;

namespace Deaven.CombatSystem
{
    public class Damagedealer : SerializedMonoBehaviour
    {
        public IDamageActor DamageActor;
        public IDamageable CurrentTarget;

        [Button]
        public virtual void Attack()
        {
            DamageActor.Attack(CurrentTarget);
        }
    }
}