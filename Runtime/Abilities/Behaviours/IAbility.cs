using Deaven.CombatSystem.Abilities;

namespace Deaven.CombatSystem
{
    public interface IAbility
    {
        public int BaseIncreaseInPercent { get; set; }
        public int TotalAmount { get; set; }
        public bool CanUse { get; set; }
        public float Cooldown { get; set; }
        public string Description { get; set; }
        public AbilityType Type { get; set; }
        public Damagedealer Owner { get; set; }
        public void Activate();
        public void StartCooldown();
    }
}