using System;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Deaven.CombatSystem.Abilities
{
    public abstract class Ability : MonoBehaviour, IAbility
    {
        [SerializeField] protected AbilityData abilityData;
        [SerializeField] protected Damagedealer owner;

        protected int totalAmount;
        
        private string description;

        [ShowInInspector, ReadOnly] public int BaseIncreaseInPercent { get; set; }
        [ShowInInspector, ReadOnly] public int BaseDamage { get; set; }
        [ShowInInspector, ReadOnly] public int Range { get; set; }

        [ShowInInspector, ReadOnly]
        public virtual int TotalAmount
        {
            get
            {
                int value = 0;

                value = abilityData.BaseDamage + (int)((abilityData.BaseDamage / 100f) * BaseIncreaseInPercent);

                return value;
            }
            set => totalAmount = value;
        }

        [ShowInInspector, ReadOnly] public bool CanUse { get; set; }
        [ShowInInspector, ReadOnly] public float Cooldown { get; set; }
        [ShowInInspector, ReadOnly] public AbilityType Type { get; set; }

        [ShowInInspector, ReadOnly]
        public string Description
        {
            get
            {
                if (!abilityData)
                    return "";
                
                string[] words = abilityData.Description.Split(' ');
                List<string> newDescription = new List<string>();

                string damagePercentageKeyword = "$damagePercentage";
                string baseDamageKeyword = "$baseDamage";
                string cooldownKeyword = "$cooldown";
                string rangeKeyword = "$range";
                
                foreach (string word in words)
                {
                    if (word == damagePercentageKeyword)
                    {
                        newDescription.Add($"<color=\"green\"><b>{BaseIncreaseInPercent}%</b></color>");
                        continue;
                    }

                    if (word == cooldownKeyword)
                    {
                        newDescription.Add($"<color=\"blue\"><b>{Cooldown} seconds</b></color>");
                        continue;
                    }
                    
                    if (word == baseDamageKeyword)
                    {
                        newDescription.Add($"<color=\"yellow\"><b>{BaseDamage}</b></color>");
                        continue;
                    }
                    
                    if (word == rangeKeyword)
                    {
                        newDescription.Add($"<color=\"pink\"><b>{Range}</b></color>");
                        continue;
                    }

                    newDescription.Add(word);
                }

                return String.Join(" ", newDescription);
            }
            set => description = value;
        }

        public virtual Damagedealer Owner
        {
            get => owner;
            set
            {
                owner = value;
            }
        }

        private void Start()
        {
            CanUse = true;
            Cooldown = abilityData.BaseCooldown;
            BaseIncreaseInPercent = abilityData.BaseIncreaseInPercent;
            BaseDamage = abilityData.BaseDamage;
            Type = abilityData.Type;
            Owner = owner;
        }

        [Button]
        public abstract void Activate();

        public void StartCooldown()
        {
            CanUse = false;
            DOVirtual.DelayedCall(Cooldown, () => CanUse = true);
        }
    }
}