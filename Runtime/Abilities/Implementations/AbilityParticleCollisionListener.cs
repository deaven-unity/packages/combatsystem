using System;
using System.Collections.Generic;
using UnityEngine;

namespace Deaven.CombatSystem.Abilities
{
    [RequireComponent(typeof(ParticleSystem))]
    [RequireComponent(typeof(AbilityData))]
    public class AbilityParticleCollisionListener : MonoBehaviour
    {
        [SerializeField] private IDamageActor damageActor;
        
        [SerializeField] public event Action Collided;
        
        private ParticleSystem particle;
        private List<ParticleCollisionEvent> collisionEvents;
        private IAbility ability;

        private void Awake()
        {
            particle = GetComponent<ParticleSystem>();
            ability = GetComponent<IAbility>();
        }

        private void Start()
        {
            collisionEvents = new List<ParticleCollisionEvent>();
        }

        private void OnParticleCollision(GameObject other)
        {
            int numCollisionEvents = particle.GetCollisionEvents(other, collisionEvents);
            
            for (int i = 0; i < numCollisionEvents; i++)
            {
                SendDamageMessage(other);
                Collided?.Invoke();
            }
        }
        private void SendDamageMessage(GameObject other)
        {
            IDamageable d = other.GetComponent<IDamageable>();

            if (d == null)
                return;

            var msg = new DamageMessage
            {
                DamageActor = damageActor,
                Amount = ability.TotalAmount
            };

            if (ability.Type == AbilityType.Damage)
                d.TakeDamage(msg);
            else if (ability.Type == AbilityType.Heal)
                d.Heal(msg);
        }
    }
}