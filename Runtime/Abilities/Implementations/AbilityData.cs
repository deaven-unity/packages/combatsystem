using System;
using System.Collections.Generic;
using System.ComponentModel;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Deaven.CombatSystem.Abilities
{
    [CreateAssetMenu(fileName = "Ability", menuName = "CombatSystem/Abilities/Ability", order = 0)]
    public class AbilityData : ScriptableObject
    {
        [SerializeField] private int baseIncreaseInPercent;
        [SerializeField] private int baseDamage;
        [SerializeField] private float baseCooldown;
        [SerializeField] private AbilityType abilityType;
        [SerializeField] private new string name;

        [SerializeField, TextArea]
        [InfoBox("Keywords $damagePercentage, $baseDamage, $cooldown, $range")]
        private string description;

        [SerializeField, PreviewField] private Sprite icon;
        [SerializeField] protected GameObject prefab;

        public string Name => name;

        public Sprite Icon => icon;

        public GameObject Prefab => prefab;

        public float BaseCooldown => baseCooldown;

        public int BaseIncreaseInPercent => baseIncreaseInPercent;

        public AbilityType Type => abilityType;

        public string Description => description;

        public int BaseDamage => baseDamage;
    }
}