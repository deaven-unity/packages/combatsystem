using System.Collections.Generic;
using UnityEngine;

namespace Deaven.CombatSystem.Abilities
{
    [CreateAssetMenu(fileName = "AbilitySet", menuName = "CombatSystem/Abilities/AbilitySet", order = 0)]
    public class AbilitySet : ScriptableObject
    {
        public List<AbilityData> abilities;
    }
}