namespace Deaven.CombatSystem.Abilities
{
    public enum AbilityType
    {
        Damage,
        Heal
    }
}